f = open('tallfil.txt','r')
innhold = f.readlines()
f.close()
print(f'innhold:\n{innhold}\n')
# Konverter til tall
tall = []
for i in innhold:
    tall.append(int(i.strip()))
print(tall)
# Eller:
for i in range(len(innhold)):
    innhold[i] = int(innhold[i].strip())
print(innhold)