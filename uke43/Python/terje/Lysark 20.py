filnavn = input('Oppgi navn på fila: ')
f = open(filnavn,'w')

print('Skriv inn tekst som skal lagres til {filnavn}')
print('Avslutt med å trykke enter (uten tekst)')

tekst = 'noe'      # For å komme inn i while-løkka
enter = '\n'       # For linjeskift

while tekst != '':
    tekst = input('> ')
    if tekst != '':
        f.write(tekst+enter)
f.close()
print(f'Teksten er nå lagret i {filnavn}')

f = open(filnavn,'r')
innhold = f.readlines()
f.close()
print(innhold)