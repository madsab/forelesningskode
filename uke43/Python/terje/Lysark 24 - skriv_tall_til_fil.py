filnavn = input('Oppgi navn på fila: ')
f = open(filnavn,'w') # Åpner fila for skriving

print(f'Skriv inn positive tall og tallet i andre lagres til {filnavn}')
print('Avslutt med å skrive -1')
tall = 0 # For at while-lokka skal få True

while tall!=-1:
    tall = int(input('> '))
    if tall != -1:
        f.write(f'{tall}\t{tall**2}\n')

f.close()

print(f'Tallene har nå blitt lagret i {filnavn}')