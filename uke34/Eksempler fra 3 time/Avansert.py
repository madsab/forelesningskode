import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
# t blir vektoren [0.00, 0.01, 0.02, 0.03,...1.99]
s = 1 + np.sin(2 * np.pi * t)
# her regnes 2*pi*t + 1 for hver verdi av t og dette legges inn i vektoren s

# fig, ax = plt.subplots()
# genererer grafen og returnerer en referanse til figuren (til fig)
# og b.l.a. en liste over aksene (ax)

plt.plot(t, s)
# plotter selve grafen

plt.xlabel('time (s)')
plt.ylabel('voltage (mV)')
plt.title('About as simple as it gets, folks')
# setter navn på aksene

plt.grid()
# tegner en grid på grafen

#fig.savefig("test.png")       # Lagrer grafen som en png-fil 
plt.show()                    # Viser vinduet med grafen