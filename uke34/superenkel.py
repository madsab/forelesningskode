# Vi lager en ny variabel kalt a, og setter den til 3*4

a = 3*4
print(a)

# Hvis du bruker Thonny kan du nå kjøre filen.
# Da skal 12 skrives ut nede siden vi ba om å skrive verdien av a
# Hvis du så skrive a <linjeskift> i det nederste vinduet, så ser du
# at vi nå har lagret en variabel a med verdi 12.
