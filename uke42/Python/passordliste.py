'''
Dette er en veldig enkel greie å vise hvordan en kan
bruke todimensjonale lister for å holde rede på kobling
mellom to dataverdier. 
'''

# Leter igjennom passordliste, etter treff
def finn_passord(passordliste):
    nettsted = input('Nettsted du vil ha passord til: ')
    for sted in passordliste:
        print(sted)
        if sted[0] == nettsted:
            return sted[1] # Dette er jo passordet
    return None # Default


liste = [['reddit', '''¯\_(ツ)_/¯'''], ['youtube', 'passordYT!'], \
         ['ute.no', 'O$teSkrakkeleringsGnøffe_Gnøff'], \
         ['filateli.no', 'Frimerker4ever']]


d = {}
for sted in liste:
    d[sted[0]] = sted[1]
print(sted)


for k, v in d.items():
    
#    print(f'passordet til {key} er {d[key]}.')
    print(key, value)

# print('finn_passord: ' + finn_passord(liste))