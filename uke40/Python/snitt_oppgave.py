# Oppgave 1:
# Skriv funksjonen lag_tall_liste()
# Inneholder en løkke der brukeren blir bedt om å skrive inn heltall.
# Hvert tall skal legges til i en liste. Når brukeren taster inn 0 skal
# listen med alle tallene som er lagt inn til nå returneres. Se eksempel

def lag_tall_liste():

    liste = []

    svar = int(input("tall: "))
    while svar:
        liste.append(svar)
        svar = int(input("tall: "))
        print("lagt til",svar)
    return liste
    
# Dette funker kun hvis du har Python 3.8 eller senere!
# https://realpython.com/python-walrus-operator/
def ltl():
    liste = []
    while svar := int(input("tall: ")):
        liste.append(svar)
    return liste


def ltl2():
    liste = []
    
    while True:
        tall = int(input("tall: "))
        if tall == 0:
            return liste
        else:
            liste.append(tall)


print(ltl())

# Eks:
# print(lag_tall_liste())
# [2, 3, 11, -1, 122]

# Oppgave 2
# Skriv funksjonen beregn_snitt():
# Denne funksjonen skal ta inn en liste som parameter,
# beregne gjennomsnittet av alle tall i listen, og returnere
# dette snittet. Se eksempel

def beregn_snitt(liste):
    return sum(liste)/len(liste)


# print(gjennomsnitt([2, 3, 11, -1, 122]))
# 27.4


# Oppgave 3
# Skriv funksjonen topp_og_bunn().
# Funksjonen tar inn en liste, og returnerer TO ting:
# Laveste og høyeste verdi i listen. Se eksempel

def topp_og_bunn(liste):
    return min(liste), max(liste)

print(topp_og_bunn([2, 3, 11, -1, 122]))
# (-1, 122)
